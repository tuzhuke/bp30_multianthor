基于51uwb新框架实现的TWR定位
具体参考网址:http://51uwb.cn/forum.php?mod=viewthread&tid=402&extra=page%3D1

工程编译说明
1TWR定位共有两种节点，标签和基站
标签是被定位点，基站是固定参考点。

标签和基站的编译是通过宏定义区分的，修改文件bphero_uwb.h

！！！重要 ： 如果使用数据融合部分，标签使用带外壳的，带MPU9250 传感器的底板！！！


=====================下面实例编译基站0x0002===============
#define RX_NODE
//#define TX_NODE

#ifdef RX_NODE
 #define SHORT_ADDR 0x0002
 #define LCD_ENABLE 
#endif  


#ifdef TX_NODE
 #define SHORT_ADDR 0x000A
#endif  
#define NET_PANID 0xF0F0
==========================END==============================

基站三个地址固定，分辨是0x0001 0x0002 和0x0003，标签地址无需更改，默认即可

定位过程，需要通过基站0x0001的串口连接电脑，将距离数据送到电脑。
更多内容参考论坛链接说明

本程序定位程序由www.51uwb.cn网站发布
程序主体为DecaWare TWR 测距程序，在此基础上由www.51uwb.cn 修改

任何程序更新将在www.51uwb.cn公布而不另行通知，请及时关注网站信息

更多程序、模块配置使用说明请参见www.51uwb.cn网站。
