/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include <stdio.h>
#include "deca_device_api.h"
#include "deca_sleep.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
static unsigned char distance_seqnum = 0;
static srd_msg_dsss *msg_f_recv ;
typedef signed long long int64;
typedef unsigned long long uint64;
typedef unsigned int uint32;
/*******************************************************************************************************************/
//以下6个变量用来记录时间戳信息
static uint64 poll_rx_ts;
static uint64 resp_tx_ts;
static uint64 final_rx_ts;
static uint64 poll_tx_ts;
static uint64 resp_rx_ts;
static uint64 final_tx_ts;
static void Send_Dis_To_Anthor0(void);//函数声明，将三个距离信息发送给地址为0x0001的基站
//anthor range
#define SEPC_ADDRESS 0x0000   //特殊地址，当循环到这个地址时，将三个距离信息发送给0x0001基站
#define DEST_BEGIN_ADDR 0x0001 //测距基站起始地址
#define DEST_END_ADDR   DEST_BEGIN_ADDR + MAX_ANTHOR - 1 //anthro address 0x001 0x002 0x003 for 2D ,0x0001 0x0002 0x0003 0x0004 for 3D
uint16 Dest_Address =  DEST_BEGIN_ADDR; //目标基站地址，默认从开始(0x0001基站)执行

int Final_Distance[MAX_ANTHOR] = {0};//save all the distance from anthor and tag

char dist_str[16] = {0};//用来存放LCD液晶显示的字符串
//unsigned long time_count = 0;
unsigned long Tag_receive_poll = 0; //全局变量标记是否接收到错误帧干扰

char isMpu9250_moved = 0;

struct Anthor_Information
{
    uint16 short_address;//基站16bit 短地址
    uint16 distance;//距离信息，高8 低8bit
    uint32 last_time;//上次通信时间
    uint8  rssi_info;//上次通信RSSI值记录
    unsigned char alive; //是否已经识别或者是否已经丢失

} anthor_info[MAX_ANTHOR];
uint8 gSend_index = 0;
uint8 gProcess_Dis = 0;


/*******************************************************************************
* 函数名  : BPhero_TAG_Broadcast
* 描述    : 标签启动发送广播信息给各个基站,信息数据包包括了基站短地址
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 发送broadcast信息(B信息)给所有基站
*******************************************************************************/

void BPhero_TAG_Broadcast(void)
{
    uint8 index = 0 ;
    uint8 strlen = 0;
    uint8 anthor_count = 0;
    msg_f_send.destAddr[0] = 0xFF;
    msg_f_send.destAddr[1] = 0xFF;

    msg_f_send.seqNum = distance_seqnum;
    msg_f_send.messageData[0]='B';//broadcast message
    strlen = strlen + 2;

    uint8 *pAnthor_Str = &msg_f_send.messageData[2];
    //后面跟基站信息
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 1)
        {
//            sprintf(pAnthor_Str, "%04X:",anthor_info[index].short_address);
//            pAnthor_Str = pAnthor_Str + 5;
//            strlen = strlen + 5;
//					  sprintf(pAnthor_Str, "%04X:",anthor_info[index].short_address);
            pAnthor_Str[0] = anthor_info[index].short_address & 0xFF;
            pAnthor_Str[1] = anthor_info[index].short_address >>8;
            pAnthor_Str[2] = ':';
            pAnthor_Str = pAnthor_Str + 3;
            strlen = strlen + 3;

            anthor_count++;
        }
    }
    msg_f_send.messageData[1] = anthor_count;
    //GPIOB.5设定，兼容之前带PA的模块-->如需求请联系www.51uwb.cn
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, !GPIO_PIN_RESET);//PA node ,enable pa
    //写入数据
    dwt_writetxdata(11 + strlen,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    dwt_writetxfctrl(11 + strlen, 0);
    dwt_starttx(DWT_START_TX_IMMEDIATE);	//启动发送
    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))	//等待发送完成
    { };
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);//清除发送完成标志
    poll_tx_ts = get_tx_timestamp_u64();//读取发送时间戳

    //清空接收缓存，待收到数据时使用
    for (int i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    dwt_enableframefilter(DWT_FF_DATA_EN);	//启动帧过滤功能
    dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS*10);//设定接收延时函数
    dwt_rxenable(0);//启动接收机
    //sequence控制
    if(++distance_seqnum == 255)
        distance_seqnum = 0;
}


uint8 Update_Anthor_Info(uint32 shortaddress)
{
    uint8 index = 0;
    printf("shortaddress = 0x%04X\n",shortaddress);
    //后面跟基站信息
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 0)
        {
            anthor_info[index].short_address = shortaddress;
            anthor_info[index].alive = 1;
            return 1;
        }
    }
    return 0;
}

uint8 Count_Anthor()
{
    uint8 index = 0;
    uint8 count = 0;
    //后面跟基站信息
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 1)
        {
            count++;
        }
    }
    return count;
}


uint16 Find_Address()
{
    uint8 index = 0;
    uint8 count = 0;
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 1)
        {
            if(count == gSend_index)
                return anthor_info[index].short_address;
            count++;
        }
    }
}

void Delete_Anthor(uint16 address)
{
    uint8 index = 0;
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].short_address == address)
        {
            anthor_info[index].alive = 0;
        }
    }
}

uint8 Add_Distance(uint16 address, uint16 distance)
{
    //printf("Distance = %d, Address = 0x%04X\n",distance,address);
    uint8 index = 0;
    for(index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].short_address == address)
        {
            anthor_info[index].distance = distance;
            anthor_info[index].rssi_info = 0x11;
            return 1;
        }
    }
    return 0;
}

/*******************************************************************************
* 函数名  : Tx_Simple_Rx_Callback
* 描述    : TX 节点DW1000 中断回调函数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 当节点DW1000收到信息后会触发中断调用到这个函数
*******************************************************************************/
void Tx_Simple_Rx_Callback()
{
    uint32 status_reg = 0,i=0;
    uint32 final_tx_time;
    uint16 address ;
    //printf("callback debug1\n");
    dwt_enableframefilter(DWT_FF_NOTYPE_EN);//关闭帧过滤功能
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);//读取状态表示
    if (status_reg & SYS_STATUS_RXFCG)//正确收到数据，且通过帧过滤
    {
        //printf("callback debug2\n");
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;//获取接收到数据长度
        if (frame_len <= FRAME_LEN_MAX)//数据长度判定
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);//将接收到的数据读取到rx_buffer中
            msg_f_recv = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f_recv->sourceAddr[0];//提取该数据的发送节点短地址
            msg_f_send.destAddr[1] = msg_f_recv->sourceAddr[1];
            //	printf("callback debug3\n");
            msg_f_send.seqNum = msg_f_recv->seqNum;//提取该数据的发送seq num

            switch(msg_f_recv->messageData[0]) //通过用户自定义的第一个数据判定为何种信息
            {

            case 'R':
                address = msg_f_recv->sourceAddr[1]<<8|msg_f_recv->sourceAddr[0];
                printf("receive R message 0x%04X\n",address);
                Update_Anthor_Info(address);
                break;

            //基站发送的应答信息(基站收到标签发送的Poll信息后，会给标签回复Ack应答信息)
            case 'A'://Poll ack message
            {
                //	printf("callback debug4\n");
                resp_rx_ts = get_rx_timestamp_u64();//读取接收到该信息的时间戳
                //发送一条delayed 信息(定时发送)
                //定时发送的好处是可以将 这条信息发送的具体时间  打包到这个数据包发送给对方
                final_tx_time =   dwt_readsystimestamphi32()  + 0x17cdc00/80;//10ms/8
                dwt_setdelayedtrxtime(final_tx_time);
                final_tx_ts = (((uint64)(final_tx_time & 0xFFFFFFFE)) << 8);
                msg_f_send.messageData[0]='F';//Final message
                final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_POLL_TX_TS_IDX], poll_tx_ts); //POLL 信息时间戳
                final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_RESP_RX_TS_IDX], resp_rx_ts); //ACK  信息时间戳
                final_msg_set_ts(&msg_f_send.messageData[FINAL_MSG_FINAL_TX_TS_IDX], final_tx_ts);//定时信息时间戳
                //printf("callback debug5\n");

                //将所有时间戳进行打包写入到DW1000中
                dwt_writetxdata(25, (uint8 *)&msg_f_send, 0) ; // write the frame data
                dwt_writetxfctrl(25, 0);
                //启动发送-->不会立即完成，到定时器设定的时间才发送
                dwt_starttx(DWT_START_TX_DELAYED);
                //printf("callback debug6\n");
                //ACK 信息其它部分，包含了上次测距的距离信息
                //这样做的好处可以减少一条单独发送距离信息
                //第一次系统启动的时候，基站第一次回复ACK的时候还没有测距，此时的距离信息无效，后期回复的ACK信息包含了上次测距的距离信息
                Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1] = (msg_f_recv->messageData[1]*100 + msg_f_recv->messageData[2]);//cm
                //Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1] = filter(Final_Distance[(msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1],\
                //     (msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0] - 1);
                //	printf("callback debug7\n");

                if(Add_Distance((msg_f_send.destAddr[1]<<8)|msg_f_send.destAddr[0],msg_f_recv->messageData[1]*100 + msg_f_recv->messageData[2]) == 0)
                    printf("Add distance fail\n");
//								else
//									   printf("Add distance pass\n");
                gSend_index++;
                //等待定时信息发送完成
                while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
                { };
                dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);//清除发送完成标志
                //	printf("callback debug8\n");



            }
            break;
            default:
                break;
            }
        }
    }
    else
    {
        if(gProcess_Dis == 1)
        {
            printf("timeout address 0x%04X\n",Find_Address());
            Delete_Anthor(Find_Address());
        }
        if(!(status_reg & SYS_STATUS_RXRFTO))//timeout以外都认为是数据帧干扰
            Tag_receive_poll = 1;//数据帧干扰
        //清理接收标识
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
    }
    if( Dest_Address == DEST_BEGIN_ADDR)
    {
    }
}

/*******************************************************************************
* 函数名  : BPhero_Distance_Measure_Specail_ANTHOR
* 描述    : 标签启动TWR测距请求，基站地址为Dest_Address
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 发送Poll信息(P信息)给特定地址基站启动测距
*******************************************************************************/

void BPhero_Distance_Measure_Specail_ANTHOR(void)
{
    uint16 destaddress = Find_Address();
   // printf("Send Index = %d, Address = 0x%04X\n",gSend_index,destaddress);
    msg_f_send.destAddr[0] =(destaddress) &0xFF;
    msg_f_send.destAddr[1] =  ((destaddress)>>8) &0xFF;

    msg_f_send.seqNum = distance_seqnum;
    msg_f_send.messageData[0]='P';//Poll message

    //GPIOB.5设定，兼容之前带PA的模块-->如需求请联系www.51uwb.cn
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, !GPIO_PIN_RESET);//PA node ,enable pa
    //写入数据
    dwt_writetxdata(12,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    dwt_writetxfctrl(12, 0);
    dwt_starttx(DWT_START_TX_IMMEDIATE);	//启动发送
    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))	//等待发送完成
    { };
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);//清除发送完成标志
    poll_tx_ts = get_tx_timestamp_u64();//读取发送时间戳

    //清空接收缓存，待收到数据时使用
    for (int i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    dwt_enableframefilter(DWT_FF_DATA_EN);	//启动帧过滤功能
    dwt_setrxtimeout(RESP_RX_TIMEOUT_UUS);//设定接收延时函数
    dwt_rxenable(0);//启动接收机
    //sequence控制
    if(++distance_seqnum == 255)
        distance_seqnum = 0;
}


/*******************************************************************************
* 函数名  : TAG_SendOut_Messge
* 描述    : 标签周期性启动数据收发
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 1标签和基站测距
						2标签将距离信息发送给基站0x0001
*******************************************************************************/
//void TAG_SendOut_Messge(void)
//{
//    if(Dest_Address == SEPC_ADDRESS)//SPEC_ADDRESS = 0，当归零后说明完成一个循环的测距，将一个循环里的距离发送给基站0x0001
//    {
//        Send_Dis_To_Anthor0();
//    }
//    else
//    {
//        BPhero_Distance_Measure_Specail_ANTHOR();//和特定地址基站进行测距(特定地址即Dest_Address)
//    }
//    Dest_Address++;
//    if(Dest_Address == DEST_END_ADDR+1)//地址自增后归零
//    {
//        Dest_Address = SEPC_ADDRESS;
//    }
//}
/************************************************************************/
/************************************************************************/
/************************使用定时器周期性启动测距************************/
/********************************BEGIN***********************************/
/************************************************************************/
TIM_HandleTypeDef  htim3;
#define  MAX_FREQ_HZ 1     //刷新频率设定定位2HZ，需要与3个节点测试2次，共计需要6次,间隔大约166ms
#define  MAX_TX_Node 5    //标签个数设定，系统中最大标签个数
#define  TIM3_ReLoad       (int)(10000/(MAX_FREQ_HZ*MAX_ANTHOR))//定时器reload 间隔，1发送节点2hz reload about 166ms
#define  TIM3_Delay_Step   (int)((TIM3_ReLoad)/MAX_TX_Node)     //多个tx节点，每个节点之间的delay
/*******************************************************************************
* 函数名  : MX_TIM3_Init
* 描述    : 定时器3初始化
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 定时器3初始化，通过reload 配置定时器周期
*******************************************************************************/
static void MX_TIM3_Init(uint32 reload)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 8400-1;//10HZ freq
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = reload;
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }

    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
    {
        _Error_Handler(__FILE__, __LINE__);
    }
}
/*******************************************************************************
* 函数名  : HAL_TIM_PeriodElapsedCallback
* 描述    : 定时器3中断回调函数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 定时器3初始化，通过reload 配置定时器周期
*******************************************************************************/
//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
//{
//    if (htim->Instance == htim3.Instance)
//    {
//        HAL_TIM_Base_Stop(&htim3);
//        if( Tag_receive_poll ==  0)//没有发现数据冲突，正常发送数据
//        {
//            dwt_forcetrxoff();
//            TAG_SendOut_Messge();
//            if(Dest_Address == DEST_BEGIN_ADDR)
//            {
//                HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4);
//            }
////            time_count = portGetTickCnt();
//            TIM3->ARR = TIM3_ReLoad;
//        }
//        else//发现数据冲突，随机进行微调delay,调整下次启动发送时间从而错开冲突
//        {
//            HAL_TIM_Base_Stop(&htim3);
//            TIM3->ARR = TIM3_Delay_Step*((SHORT_ADDR%10)+1);//random delay
//            Tag_receive_poll = 0;
//        }
//        HAL_TIM_Base_Start(&htim3);
//    }
//}



void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == htim3.Instance)
    {
        HAL_TIM_Base_Stop(&htim3);
        {
            dwt_forcetrxoff();
            if(Count_Anthor() < 4)
            {
                gProcess_Dis = 0;
                BPhero_TAG_Broadcast();
                gSend_index = 0;
							//	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
								isMpu9250_moved = 0;
								HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, !GPIO_PIN_SET); //PB7 = 1 LED_ON

            }
            else
            {
                if(gSend_index ==Count_Anthor())
                {
                    gSend_index= 0;
                    Send_Dis_To_Anthor0();
										isMpu9250_moved = 0;
										HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, !GPIO_PIN_SET); //PB7 = 1 LED_ON

                } else
                {
                    gProcess_Dis = 1;
                    BPhero_Distance_Measure_Specail_ANTHOR();// 从1 2 3 4发送
									//	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);

                }
            }
        }
        TIM3->ARR = TIM3_ReLoad;
        HAL_TIM_Base_Start(&htim3);
    }
}

/************************使用定时器周期性启动测距************************/
/*********************************END***********************************/


/************************************************************************/
/************************************************************************/
/************************使用定LCD 显示调试距离信息***********************/
/*********************调试完毕关闭LCD显示提高刷新频率*********************/
/********************************BEGIN***********************************/
/************************************************************************/
/*******************************************************************************
* 函数名  : LCD_Display_Distance
* 描述    : 标签液晶显示距离信息
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 如果为了加速测距或者定位，请将液晶显示部分注释掉
*******************************************************************************/
static void LCD_Display_Distance(void)
{
		uint8 lcd_index= 0;
		char dist_str[16] = {0};
	  for(uint8 index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 1)
        {
            sprintf(dist_str, " %04X : %04d cm ",anthor_info[index].short_address,anthor_info[index].distance);
						OLED_ShowString(0, 2*lcd_index,dist_str);
						lcd_index++;
        }
    }    
//    if(Final_Distance[0]>0)//标签和基站0x0001 的距离
//    {
//        sprintf(dist_str, "an1:%3.2fm      ", (float)Final_Distance[0]/100);
//        OLED_ShowString(0, 2,dist_str);
//    }
//    if(Final_Distance[1]>0)//标签和基站0x0002 的距离
//    {
//        sprintf(dist_str, "an2:%3.2fm      ", (float)Final_Distance[1]/100);
//        OLED_ShowString(0, 4,dist_str);
//    }
//    if(Final_Distance[2]>0)//标签和基站0x0003 的距离
//    {
//        sprintf(dist_str, "an3:%3.2fm      ", (float)Final_Distance[2]/100);
//        OLED_ShowString(0, 6,dist_str);
//    }
}
/**********************************END***********************************/
/************************************************************************/

//**************************************************************//
//distance1 anthor0 <--> TAG  cm
//distance2 anthor1 <--> TAG  cm
//distance3 anthor2 <--> TAG  cm
//将三个距离信息发送给基站1--> 0x0001
//如果用其它无限模块通信，可以将这三个距离信息通过其它无线模块传输给电脑
//**************************************************************//
/*******************************************************************************
* 函数名  : Send_Dis_To_Anthor0
* 描述    : 标签将三个收到的三个距离信息打包发送到基站0(基站0的SHORT地址0x0001)
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 1将距离信息发送到0x0001地址的基站
						2通过液晶显示-->只能显示三个距离，液晶每行一个距离
*******************************************************************************/
static void Send_Dis_To_Anthor0(void)
{
    static int framenum = 0 ;
    char crc_byte = 0;
    //only send this message to anthor0:short address equal 0x0001
    msg_f_send.destAddr[0] = 0xFF;
    msg_f_send.destAddr[1] = 0xFF;

    msg_f_send.seqNum = distance_seqnum;

    msg_f_send.messageData[0]='M';
		msg_f_send.messageData[1] = 0;//数据包长度
	
    uint8 *pAnthor_Str = &msg_f_send.messageData[2];
    int str_len = 0x20;
		sprintf(pAnthor_Str, "&&&:%02X$%04X:%d:%02X$",str_len,SHORT_ADDR,isMpu9250_moved,msg_f_send.seqNum);//AA55 ANTHORID
   
		pAnthor_Str = pAnthor_Str + 15+2;
		isMpu9250_moved = 0;
    for(uint8 index = 0 ; index < MAX_ANTHOR; index++)
    {
        if(anthor_info[index].alive == 1)
        {
            sprintf(pAnthor_Str, "%04X:%04X:%02X#",anthor_info[index].short_address,anthor_info[index].distance,anthor_info[index].rssi_info);
            pAnthor_Str = pAnthor_Str + 13;
        }
    }
    pAnthor_Str = pAnthor_Str - 1;
    sprintf(pAnthor_Str, "$AA##\r\n");

    while(msg_f_send.messageData[str_len] != '\n')
    {
        crc_byte =crc_byte^msg_f_send.messageData[str_len];
        str_len++;
    }
		str_len++;//字符串最后追加'\n'
    printf(&msg_f_send.messageData[2]);
		msg_f_send.messageData[1] = str_len - 2;//有用数据，其他模块需要传输到串口的数据
		
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, !GPIO_PIN_RESET);//PA node ,enable pa
    dwt_writetxdata(11 + str_len,(uint8 *)&msg_f_send, 0) ;  // write the frame data
    dwt_writetxfctrl(11 + str_len, 0);
    dwt_starttx(DWT_START_TX_IMMEDIATE);
    while (!(dwt_read32bitreg(SYS_STATUS_ID) & SYS_STATUS_TXFRS))
    { };
                dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS);
    framenum++;
    LCD_Display_Distance();
}

/*******************************************************************************
* 函数名  : tx_main
* 描述    : 标签主函数
* 输入    : 无
* 输出    : 无
* 返回值  : 无
* 说明    : 1主要通过液晶显示信息
						2注册回调函数
						3使用定时器周期性发送信息
*******************************************************************************/
int tx_main(void)
{
    OLED_ShowString(0,0,"   51UWB Node");
    OLED_ShowString(0,2, "    Tx Node");
    OLED_ShowString(0,6,"  www.51uwb.cn");

    bphero_setcallbacks(Tx_Simple_Rx_Callback);//注册DW1000中断回调函数
    //定时器周期性和基站测距,改变定时器周期可以改变测距/定位刷新频率
    MX_TIM3_Init(TIM3_ReLoad);
    HAL_TIM_Base_Start_IT(&htim3); 
	  Mpu9250_Test();
    /* Infinite loop */
    while(1)
    {
         IWDG_Feed();
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
